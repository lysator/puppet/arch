# denna klass installerar cron.
class arch::cron {
  package {
    [
      'cronie',
    ]:
      ensure => installed,
  }
  -> service {
    'cronie':
      ensure => 'running',
      enable => true,
      pattern => 'cronie',
  }
}

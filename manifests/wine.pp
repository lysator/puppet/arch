# Installs Wine
class arch::wine {
  include arch::multilib

  ensure_packages([
    'wine',
    'wine-mono',
  ])
}

#denna klass ser till så att användare kan logga in och så att rötter kan sudoa
class arch::inloggning {

  package {
    [
      'krb5',
      'pam',
      'sssd',
      'nfs-utils',
      'sudo',
    ]:
      ensure => installed,
  }
  file { '/lysator':
    ensure => 'link',
    target => '/mp/lysator',
  }
  file { '/var/mail':
    ensure => 'link',
    target => '/mp/mail',
    force  => true,
  }
  file { '/etc/sssd/sssd.conf':
    source => 'puppet:///modules/arch/inloggning/sssd.conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
    force  => 'true',
  }
}

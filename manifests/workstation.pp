#denna klass lägger till grafiska program och fönstermiljöer
class arch::workstation {

  package {
    [
      'dmenu',
      'firefox',
      'i3-wm',
      'i3blocks',
      'i3status',
      'xfce4',
      'xfce4-goodies',
      'xorg-fonts-misc',
      'xorg-server',
      'xorg-server-common',
      'xpra',
    ]:
      ensure => installed,
  }
}

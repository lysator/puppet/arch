#base klass, fixar autouppdateringar, ssh och script för nätverksinställningar
class arch::base {

  #remove normal linux kernel to avoid conflicts
  package {
    [
      'linux',
    ]:
      ensure => absent,
  }
  package {
    [
      'openssh',
      'linux-lts',
    ]:
      ensure => installed,
  }

  #lägg till statisk ip här

  file { '/etc/pacman.d/mirrorlist':
    ensure => file,
    source => "puppet:///${module_name}/files/mirrorlist",
  }
}

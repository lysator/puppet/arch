# Sets up the multilib repo
class arch::multilib {
  $repo_name = 'multilib'

  # TODO this should be replaced by a pacman module
  # https://git.hornquist.se/puppet/pacman
  ini_setting {
    default:
      ensure  => 'present',
      path    => '/etc/pacman.conf',
      section => $repo_name,
      ;
    "Pacman repo [${repo_name}] server":
      setting => 'Include',
      value   => '/etc/pacman.d/mirrorlist'
      ;
  } ~> exec { 'Update pacman repos':
    command     => '/usr/bin/pacman --noconfirm --noprogressbar -Sy',
    refreshonly => true,
  }
}

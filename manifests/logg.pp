# denna klass fixar loggning till loggservern..
class arch::logg {

  package {
    [
      'syslog-ng',
    ]:
      ensure => installed,
  }

  #lägg till linjer till konfigurationsfilen för loggning till loghost  
  file_line { 'Remote to logserv':
      path => '/etc/syslog-ng/syslog-ng.conf',
      line => 'destination remote { network("130.236.254.25" transport("tcp") port(514));};',
  }
  file_line { 'Source to remote':
      path => '/etc/syslog-ng/syslog-ng.conf',
      line => 'log { source(src); destination(remote); };',
  }
  -> service {
    'syslog-ng@default':
      ensure  => 'running',
      enable  => true,
      pattern => 'syslog',
  }
}

#!/bin/sh

pacman -S netctl
ip a

echo "enter interface for static IP:"
read -r interface

echo "enter static IP-address:"
read -r address

echo "enter gateway-address:"
read -r gateway

echo "enter DNS-address:"
read -r dns

rm /etc/netctl/static
touch /etc/netctl/static
echo "Interface=$interface\nConnection=ethernet\nIP=static\nAddress='$address'\nGateway='$gateway'\nDNS='$dns'" > /etc/netctl/static

echo "do you want to confige IPV6 right now?[y/n]"
while true; do
	read -r yn
	if [ "$yn" = n ]; then
		echo "#IP6=static\n#Address6='1234:1234:1234::1/64'" >> /etc/netctl/static
		echo "IP6 prefab has been put in /etc/netctl/static"
		exit
	fi
	if [ "$yn" = y ]; then
		echo "#IP6=static\n#Address6='1234:1234:1234::1/64'" >> /etc/netctl/static
		vim /etc/netctl/static
		break;
	fi
	echo "please answer y/n"
done

##generate ipv6
